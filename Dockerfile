FROM mysql:5.7
MAINTAINER FME <dev@fullmeasureed.com>

COPY fme.cnf /etc/mysql/conf.d/fme.cnf
